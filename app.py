import os
import random
import string
import argparse
import functools
import multiprocessing

from flask import Flask
from flask import redirect
from flask import request
from flask import safe_join
from flask import send_from_directory

from flask_session import Session

from flask_sqlalchemy import SQLAlchemy

from flask_login import LoginManager
from flask_login import UserMixin
from flask_login import login_user
from flask_login import logout_user
from flask_login import current_user

from flask_socketio import SocketIO

from sockets import SocketHandler

def random_string(length):
    return "".join(
        random.choice(string.hexdigits) for i in range(length)
    )

app = Flask(__name__, static_folder=None)
app.config["SECRET_KEY"] = random_string(256)
app.config["SEND_FILE_MAX_AGE_DEFAULT"] = 0
app.config["SESSION_TYPE"] = "sqlalchemy"
app.config["SQLALCHEMY_BINDS"] = {
    "sessions": "sqlite://",
}
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

app.config["SESSION_SQLALCHEMY"] = db
sess = Session(app)

login_manager = LoginManager()
login_manager.session_protection = "strong"

login_manager.init_app(app)

socketio = SocketIO(app, manage_session=False)
SocketHandler(db, socketio)

db.create_all()

class User(UserMixin):
    def __init__(self, user_id):
        self.id = user_id

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

def login_required(fn):
    @functools.wraps(fn)
    def wrapper():
        if not current_user.is_authenticated:
            return redirect("/login")
        return fn()
    return wrapper

@app.route("/login", methods=["POST"])
def validate_user():
    username = request.form.get("username")
    password = request.form.get("password")
    remember = request.form.get("remember", False)
    if username is not None:
        login_user(User(username), remember=remember)
    return redirect("/")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/login")

@app.route("/")
@app.route("/<path:path>")
def serve(path=""):
    PUBLIC_DIR = "public"
    PRIVATE_DIR = "private"
    if path == "":
        path = "index.html"
    if current_user.is_authenticated:
        if path == "login":
            return redirect("/")
    _, ext = os.path.splitext(path)
    if ext == "":
        path += ".html"
    if current_user.is_authenticated:
        loc = safe_join(PRIVATE_DIR, path)
        if os.path.exists(loc):
            return send_from_directory(PRIVATE_DIR, path)
    loc = safe_join(PUBLIC_DIR, path)
    if os.path.exists(loc):
        return send_from_directory(PUBLIC_DIR, path)
    if current_user.is_authenticated:
        return redirect("/")
    return redirect("/login")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="start a webserver"
    )
    parser.add_argument(
        "--host",
        type=str,
        nargs="?",
        help="the bind address for the server",
        default="0.0.0.0",
    )
    parser.add_argument(
        "--port",
        type=int,
        nargs="?",
        help="the bind port for the server",
        default=5000,
    )
    parser.add_argument(
        "--no-debug",
        dest="debug",
        action="store_false",
        help="run server in debug mode",
        default=True,
    )
    parser.add_argument(
        "--build",
        dest="build",
        action="store_true",
        help="build react app files and quit",
        default=False,
    )
    args = parser.parse_args()

    if args.build or not args.debug:
        os.system("cd frontend && npm run build")

    if not args.debug:
        print(f"starting server on {args.host}:{args.port}")

    extra_dirs = [
        "./frontend",
    ]

    watch_exts = [
        ".js",
        ".html",
        ".css",
    ]

    extra_files = extra_dirs[:]
    for extra_dir in extra_dirs:
        for dirname, dirs, files in os.walk(extra_dir):
            for filename in files:
                filename = os.path.join(dirname, filename)
                if os.path.isfile(filename):
                    _,ext = os.path.splitext(filename)
                    if ext in watch_exts:
                        extra_files.append(filename)

    socketio.run(
        app,
        host=args.host,
        port=args.port,
        debug=args.debug,
        extra_files=extra_files,
    )
