export default function useLogout() {
    return () => {
        window.location.href = "/logout";
    }
}
