import React from "react";
const requireModule = require.context(
    "./contexts",
    false,
    /\.js$/
);
const api = {};
const contexts = [];

requireModule.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = fileName.replace(/(\.\/|\.js)/g, "");
  api[moduleName] = {
    ...requireModule(fileName).default
  };
});

for (const [key, value] of Object.entries(api)) {
    const context = require("./contexts/" + key);
    contexts.push(context);
}

export default function ContextProvider({ children }) {
    let wrapped = <> { children } </>;
    contexts.forEach(context => {
        for (const [key, provider] of Object.entries(context)) {
            if (key.endsWith("Provider")) {
                wrapped = React.createElement(provider, {
                    children: wrapped,
                });
            }
        }
    });

    return wrapped;
}
