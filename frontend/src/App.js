import React from "react";
import useSocketIO from "./hooks/SocketIO";
import useLogout from "./hooks/Logout";

function App() {
  const socket = useSocketIO();
  const logout = useLogout();
  socket.on("connect", () => {
    console.log('connected');
  });
  return (
    <>
      <h3>hello world</h3>
      <button
        className="btn btn-secondary"
        onClick={logout}
      >
        logout
      </button>
    </>
  );
}

export default App;
