import React, { useMemo } from "react";
import socketIOClient from "socket.io-client";

export const SocketIOContext = React.createContext();

export function SocketIOProvider({ children }) {
    const socket = useMemo(socketIOClient, []);

    return (
        <SocketIOContext.Provider value={socket} >
            {children}
        </SocketIOContext.Provider>
    )
}
